<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<table border="1" cellspacing="0" cellpadding="7">

    <c:forEach var="company" items="${companies}">
        <tr>
            <td>
                    ${company.id}
            </td>
            <td>
                    ${company.name}
            </td>
            <td>
                    ${company.dateOfCreation}
            </td>
            <td>
                    ${company.personal}
            </td>
            <td>
                    ${company.foreign}
            </td>
            <td>
                <c:forEach var="specialization" items="${company.specialization}">
                    ${specialization.name}
                </c:forEach>
            </td>
        </tr>
    </c:forEach>
</table>
