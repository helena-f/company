package com.example.dao;

import com.example.HibernateConnection;
import com.example.model.Company;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class CompanyDataAccess {

    public List<Company> listCompanies() {

        List<Company> companyList = new ArrayList<>();
        Session session = HibernateConnection.getFactory();
        String hql = "from Company";
        Query query = session.createQuery(hql);
        companyList = query.list();
        return companyList;
    }
}

