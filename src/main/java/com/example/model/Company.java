package com.example.model;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "company")
public class Company implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "dateOfCreation")
    private LocalDate dateOfCreation;

    @Column(name = "personal")
    private int personal;

    @Type(type = "true_false")
    @Column(name = "isForeign")
    private boolean foreign;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "company_has_specialization",
            joinColumns = @JoinColumn(name = "company_id"),
            inverseJoinColumns = @JoinColumn(name = "specialization_id")
    )
    private List<Specialization> specializations = new ArrayList<>();

    public Company() {
    }

    public Company(int id, String name, LocalDate dateOfCreation, int personal, boolean foreign, List<Specialization> specializations) {
        this.id = id;
        this.name = name;
        this.dateOfCreation = dateOfCreation;
        this.personal = personal;
        this.foreign = foreign;
        this.specializations = specializations;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(LocalDate dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public int getPersonal() {
        return personal;
    }

    public void setPersonal(int personal) {
        this.personal = personal;
    }

    public boolean getForeign() {
        return foreign;
    }

    public void setForeign(boolean foreign) {
        this.foreign = foreign;
    }

    public List<Specialization> getSpecialization() {
        return specializations;
    }

    public void setSpecialization(List<Specialization> specialization) {
        this.specializations = specialization;
    }

}
