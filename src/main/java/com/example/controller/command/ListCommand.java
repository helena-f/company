package com.example.controller.command;

import com.example.dao.CompanyDataAccess;

import javax.servlet.http.HttpServletRequest;

public class ListCommand implements Command {

    @Override
    public void execute(HttpServletRequest request) {

            request.setAttribute("companies", new CompanyDataAccess().listCompanies());

        request.setAttribute("page", "/WEB-INF/list.jsp");
    }
}
