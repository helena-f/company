package com.example.controller.command;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;

public interface Command {
    void execute(HttpServletRequest request) throws SQLException;
}
