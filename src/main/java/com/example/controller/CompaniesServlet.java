package com.example.controller;

import com.example.controller.command.Command;
import com.example.controller.command.CommandKey;
import com.example.controller.command.ListCommand;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class CompaniesServlet extends HttpServlet {
    private Map<CommandKey, Command> commands = new HashMap<>();

    {
        commands.put(new CommandKey("GET", "/companies"), new ListCommand());
        /*commands.put(new CommandKey("GET", "/companies/add"), new GetAddCommand());
        commands.put(new CommandKey("POST", "/companies"), new AddCommand(
                commands.get(new CommandKey("GET", "/companies"))));
        commands.put(new CommandKey("GET", "/companies/search"), new SearchCommand());
        commands.put(new CommandKey("GET", "/companies/statistics"), new StatisticsCommand());*/
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Command command = commands.get(new CommandKey(req.getMethod(), req.getRequestURI()));
        try {
            command.execute(req);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        RequestDispatcher dispatcher = getServletContext()
                .getRequestDispatcher("/WEB-INF/main.jsp");
        dispatcher.forward(req, resp);
    }
}
