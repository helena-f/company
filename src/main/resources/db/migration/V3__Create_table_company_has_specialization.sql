create table if not exists company_has_specialization
(
    company_id integer NOT NULL,
    specialization_id integer  NOT NULL,
    PRIMARY KEY (company_id , specialization_id),
    FOREIGN KEY (company_id) REFERENCES company(id) on delete cascade on update cascade,
    FOREIGN KEY (specialization_id) REFERENCES specialization(id) on delete cascade on update cascade
);