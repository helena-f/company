create table if not exists specialization (
	id int not null auto_increment primary key,
	name varchar(255) not null UNIQUE
);