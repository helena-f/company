create table if not exists company (
	id int not null auto_increment primary key,
	name varchar(255) not null UNIQUE,
	dateOfCreation DATE not null,
	personal int not null default 1,
	isForeign boolean not null default false);
